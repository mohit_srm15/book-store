import 'react-native-gesture-handler';
import React from 'react';
import {StatusBar, StyleSheet, View} from 'react-native';

import { COLORS } from './src/constants/colors';
import { AppNavigationContainer } from './src/navigations/appNavigation';

const App = () => {
  return(
    <View style={styles.container}>
      <AppNavigationContainer  />
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  }
})

export default App;