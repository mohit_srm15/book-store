import React from 'react';
import { StyleSheet, TouchableOpacity, View } from 'react-native';
import { useNavigation } from '@react-navigation/core';

import { normalizePxH, normalizePxW } from '../utils/uiUtils';
import { COLORS } from '../constants/colors';
import { CustomText } from './CustomText';
import { APP_TEXTS } from '../constants/appText';

type HeaderProps = {
    title: string,
    hideBackButton?: boolean 
}

export const Header = (props: HeaderProps) => {
    const {title = '', hideBackButton = false} = props;
    const navigation = useNavigation();

    const handleBackPress = () => {
        if(!hideBackButton && navigation.canGoBack()){
            navigation.goBack();
        }
    }

    return(
        <View style={style.container}>
            {
                !hideBackButton ? 
                <TouchableOpacity style={style.backButton} onPress={handleBackPress}>
                    <CustomText variant={'semibold'} style={style.backText}>{APP_TEXTS.back}</CustomText>
                </TouchableOpacity>
                :
                null
            }
            {
                title? 
                <CustomText variant={'bold'} style={style.title}>{title}</CustomText> : null
            }
        </View>
    )
}

const style = StyleSheet.create({
    container: {
        height: normalizePxH(82),
        backgroundColor: COLORS.white,
        flexDirection: 'row',
        paddingVertical: normalizePxH(8),
        alignItems: 'flex-end',
        paddingHorizontal: normalizePxW(12),
    },
    backButton: {
        flex: 0.2,
    },
    title: {
        flex: 1,
        textAlign: 'center'
    },
    backText: {
        color: COLORS.gray
    }
})