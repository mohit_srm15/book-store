import React from 'react';
import {StyleSheet, View} from 'react-native';
import { TextInput } from 'react-native-gesture-handler';
import { COLORS } from '../constants/colors';
import { normalizePxW } from '../utils/uiUtils';

type SearchInputProps = {
    setSearchKey: (text: string) => void
    searchKey: string
}

export const SearchInput = (props: SearchInputProps) => {
    const {
        setSearchKey,
        searchKey
    } = props;
    return(
        <View style={styles.container}>
            <TextInput onChangeText={setSearchKey} value={searchKey} placeholder={'Search a Book'} />
        </View>
    )
}

const styles = StyleSheet.create({
    container:{
        padding: normalizePxW(14),
        borderRadius: normalizePxW(8),
        borderWidth: 1,
        borderColor: COLORS.gray
    }
})