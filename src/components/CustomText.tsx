import React from 'react';
import {Text, StyleSheet, TextStyle} from 'react-native';
import { normalizeFont, normalizePxH } from '../utils/uiUtils';

type CustomTextProps = {
    children: React.ReactNode,
    style?: TextStyle,
    variant: 'regular' | 'bold' | 'semibold'
}

const textStyle = StyleSheet.create({
    regular: {
        fontSize: normalizeFont(12),
        fontWeight: 'normal',
        lineHeight: normalizePxH(16)
    },
    semiBold:  {
        fontSize: normalizeFont(14),
        fontWeight: '600',
        lineHeight: normalizePxH(17)
    },
    bold: {
        fontSize: normalizeFont(16),
        fontWeight: '700',
        lineHeight: normalizePxH(19)
    }
})

const fontStyles: {[key: string]: TextStyle} = {
    'regular': textStyle.regular,
    'semiBold': textStyle.semiBold,
    'bold': textStyle.bold
}

export const CustomText = (props: CustomTextProps) => {
    const {children, style, variant='regular'} = props;
    return(
        <Text style={StyleSheet.flatten([{...fontStyles[variant]}, style])} >
            {children}
        </Text>
    )
}
