import { useNavigation } from '@react-navigation/native';
import React from 'react';
import { useRef } from 'react';
import {FlatList, StyleSheet, View, Image, TouchableOpacity} from 'react-native';
import { CustomText } from '../../../components/CustomText';

import { COLORS } from '../../../constants/colors';
import { SCREEN_NAMES } from '../../../navigations/screenNames';
import { normalizePxH, normalizePxW } from '../../../utils/uiUtils';

type BookListProps = {
    bookList: Array<any> //kept any for now
    fetchMoreData: () => void
}

export const BookList = (props: BookListProps) => {
    const {bookList, fetchMoreData} = props;
    const navigation = useNavigation<any>();

    const keyExtractor = useRef((item: any, index: number) => `${item.key}-${index}`).current;

    const renderItem = ({item}: {item: any}) => {
        const handleOnPress = () => navigation.navigate(SCREEN_NAMES.BOOK_STORE_DETAILS, {bookDetail: item})
        return(
            <TouchableOpacity style={styles.listItemContainer} onPress={handleOnPress}>
                {/* <Image source={{uri: item?.image}} style={styles.image} /> */}
                <CustomText variant={'semibold'}>{item.title}</CustomText>
            </TouchableOpacity>
        )
    }

    const listItemSeaparator = () => <View style={styles.divider} />

    return(
        <FlatList 
            renderItem={renderItem} 
            data={bookList} 
            keyExtractor={keyExtractor} 
            ItemSeparatorComponent={listItemSeaparator} 
            style={styles.container}
            onEndReached={fetchMoreData}
            onEndReachedThreshold={0.5} 
            showsVerticalScrollIndicator={false}
        />
    )
}

const styles = StyleSheet.create({
    container: {
        marginTop: normalizePxH(15)
    },
    listItemContainer: {
        paddingHorizontal: normalizePxW(16),
        paddingVertical: normalizePxH(12),
        borderWidth: 1,
        borderColor: COLORS.lightGray,
        flexDirection: 'row',
        borderRadius: normalizePxW(8)
    },
    image: {
        width: normalizePxW(40),
        aspectRatio: 1
    },
    divider: {
        height: normalizePxH(10)
    },

})