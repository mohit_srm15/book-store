import { StyleSheet } from "react-native";
import { normalizePxH, normalizePxW } from "../../../utils/uiUtils";

export const styles = StyleSheet.create({
    flex: {
        flex: 1,
    },
    container: {
        paddingHorizontal: normalizePxW(16)
    },
    searchTypeContainer: {
        flexDirection: 'row',
        marginVertical: normalizePxH(16),
        alignItems: 'center'
    },
    searchTypeContentContainer: {
        padding: normalizePxW(12),
        borderRadius: normalizePxW(8),
        marginRight: normalizePxW(10),
    }
})