import React from 'react';
import { useEffect } from 'react';
import { useRef } from 'react';
import { useState } from 'react';
import {StyleSheet, TouchableOpacity, View} from 'react-native';
import { CustomText } from '../../../components/CustomText';

import { Header } from '../../../components/Header';
import { SearchInput } from '../../../components/SearchInput';
import { API_ENDPOINTS } from '../../../constants/apiEndpoints';
import { APP_TEXTS } from '../../../constants/appText';
import { COLORS } from '../../../constants/colors';
import { BookList } from '../components/bookList';
import {styles} from './BookStoreSummary.styles';

enum BOOK_SEARCH_TYPE {
    AUTHOR = "AUTHOR",
    BOOK_NAME = "BOOK_NAME"
}

const bookSearchQueryKey = {
    [BOOK_SEARCH_TYPE.BOOK_NAME]: {
        label: 'Title',
        key: 'title'
    },
    [BOOK_SEARCH_TYPE.AUTHOR]: {
        label: 'Author',
        key: 'author'
    }
}

export const BookStoreSummary = () => {

    const [searchKey, setSearchKey] = useState<string>('');
    const [searchType, setSearchType] = useState<BOOK_SEARCH_TYPE | string>(BOOK_SEARCH_TYPE.BOOK_NAME);
    const [bookList, setBookList] = useState([]);

    const totalRecords = useRef(0);


    const getBookList = (getMoreData: boolean = false) => {
        const queryKey = searchKey.split(' ').join('+');        
        fetch(API_ENDPOINTS.SEARCH_BOOKS(bookSearchQueryKey[searchType].key, queryKey))
        .then(response => response.json())
        .then(apiRes => {
            const {numFound = 0, docs = []} = apiRes;
            totalRecords.current = numFound;
            let tempBookList: any = [];
            const bookData = docs?.map((item : any) => {
                return {
                   key: item?.key,
                   title: item?.title,
                   authorName: item?.author_name?.[0] || '',
                   firstPublishYear: item?.first_publish_year || '',
                   avgRatings: item?.ratings_average || 0,
                }
            }) || []
            if(getMoreData){
                tempBookList = [...bookList]
            }
            tempBookList.push(...bookData)
            setBookList(tempBookList)
        })
        .catch(error => {
            console.log(error);
        })
    }

    useEffect(() => {
        totalRecords.current = 0;
        if(searchKey){
            getBookList()
        }
        else{
            setBookList([]);
        }
    }, [searchKey, searchType])

    const SearchType = () => (
        <View style={styles.searchTypeContainer}>
            {
                Object.keys(bookSearchQueryKey).map((item, index) => 
                    <TouchableOpacity onPress={() => setSearchType(item)} key={`${item}-${index}`} style={StyleSheet.flatten([styles.searchTypeContentContainer, {
                        borderWidth: item === searchType ? 0 : 1,
                        borderColor: item === searchType ? COLORS.white : COLORS.lightGray,
                        backgroundColor: item === searchType ? COLORS.green : COLORS.transparent
                    }])}>
                        <CustomText style={{
                            color: item === searchType ? COLORS.white : COLORS.black
                        }} variant={'semibold'}>{bookSearchQueryKey[item].label}</CustomText>
                    </TouchableOpacity>
                )
            }
        </View>
    )

    const fetchMoreData = () => {
        if(bookList?.length < totalRecords?.current){
            getBookList(true);
        }
    }
    
    return(
        <View style={styles.flex}>
            <Header title={APP_TEXTS.bookStore} hideBackButton={true} />
            <View style={StyleSheet.flatten([styles.flex, styles.container])}>
                <SearchType />
                <SearchInput setSearchKey={setSearchKey} searchKey={searchKey} />
                <View style={styles.flex}>
                    <BookList bookList={bookList} fetchMoreData={fetchMoreData} />
                </View>
            </View>
        </View>
    )
}