import { StyleSheet } from "react-native";
import { COLORS } from "../../../constants/colors";
import { normalizePxW } from "../../../utils/uiUtils";

export const styles = StyleSheet.create({
    flex: {
        flex: 1
    },
    container: {
        margin: normalizePxW(16),
        padding: normalizePxW(12),
        borderRadius: normalizePxW(8),
        borderWidth: 1,
        borderColor: COLORS.lightGray
    },
    rowFlex: {
        flexDirection: 'row'
    },
    flex4: {
        flex: 0.4
    },
    flex6: {
        flex: 0.6
    }
})