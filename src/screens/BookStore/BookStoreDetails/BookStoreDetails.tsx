import React from 'react';
import {View} from 'react-native';
import { useRoute } from '@react-navigation/core';

import { CustomText } from '../../../components/CustomText';
import {styles} from './BookStoreDetails.styles';
import { Header } from '../../../components/Header';

export const BookStoreDetails = () => {
    const params = useRoute<any>()?.params || {};
    const bookDetail = params?.bookDetail || {};    

    return(
        <View style={styles.flex}>
            <Header title={'Book Details'} />
            <View style={styles.container}>
                <View style={styles.rowFlex}>
                    <View style={styles.flex4}>
                        <CustomText variant={'semibold'}>Name:</CustomText>
                    </View>
                    <View style={styles.flex6}>
                        <CustomText variant={'bold'}>{bookDetail?.title}</CustomText>
                    </View>
                </View>
                <View style={styles.rowFlex}>
                    <View style={styles.flex4}>
                        <CustomText variant={'semibold'}>Author:</CustomText>
                    </View>
                    <View style={styles.flex6}>
                        <CustomText variant={'bold'}>{bookDetail?.authorName}</CustomText>
                    </View>
                </View>
                <View style={styles.rowFlex}>
                    <View style={styles.flex4}>
                        <CustomText variant={'semibold'}>First Publish Year:</CustomText>
                    </View>
                    <View style={styles.flex6}>
                        <CustomText variant={'bold'}>{bookDetail?.firstPublishYear}</CustomText>
                    </View>
                </View>
                <View style={styles.rowFlex}>
                    <View style={styles.flex4}>
                        <CustomText variant={'semibold'}>Ratings:</CustomText>
                    </View>
                    <View style={styles.flex6}>
                        <CustomText variant={'bold'}>{bookDetail?.avgRatings.toFixed(2)}</CustomText>
                    </View>
                </View>
            </View>
        </View>
    )
}

