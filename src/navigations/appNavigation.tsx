import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import { SCREEN_NAMES } from './screenNames';
import { BookStoreSummary } from '../screens/BookStore/BookStoreSummary/BookStoreSummary';
import { BookStoreDetails } from '../screens/BookStore/BookStoreDetails/BookStoreDetails';

const defaultConfig = {
    headerShown: false
}

const MainStack = createStackNavigator();
export const AppNavigationContainer = () => {
    return(
        <NavigationContainer>
            <MainStack.Navigator screenOptions={defaultConfig}>
            <MainStack.Screen
                name={SCREEN_NAMES.BOOK_STORE_MAIN}
                component={BookStoreSummary}
            />
            <MainStack.Screen 
                name={SCREEN_NAMES.BOOK_STORE_DETAILS}
                component={BookStoreDetails}
            />
            </MainStack.Navigator>
        </NavigationContainer>
    )
}