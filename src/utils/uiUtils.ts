import { Dimensions, PixelRatio } from "react-native";

const { height: windowHeight, width: windowWidth } = Dimensions.get('window');
const FIGMA_WIDTH = 360;
const FIGMA_HEIGHT = 800;

const normalizePx = (dimen: number, type: 'width' | 'height' = 'width') => {
  const figmaDimenOverride = type === 'width' ? FIGMA_WIDTH : FIGMA_HEIGHT;
  const scale =
    type === 'width'
      ? windowWidth / figmaDimenOverride
      : windowHeight / figmaDimenOverride;
  return Math.round(PixelRatio.roundToNearestPixel(dimen * scale));
};


export const commonBucketPrefix = 'rider/images/';

export const normalizePxW = (dimen: number) => normalizePx(dimen, 'width');
export const normalizePxH = (dimen: number) => normalizePx(dimen, 'height');
export const normalizeFont = (dimen: number) => normalizePx(dimen, 'width');