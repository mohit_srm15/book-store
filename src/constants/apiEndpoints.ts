export const API_ENDPOINTS = {
    SEARCH_BOOKS: (queryType: string, queryKey: string) => `https://openlibrary.org/search.json?${queryType}=${queryKey}&limit=10`
}