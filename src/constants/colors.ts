export const COLORS = {
    white: 'white',
    gray: 'gray',
    lightGray: 'lightgray',
    green: 'green',
    transparent: 'transparent',
    black: 'black'
}